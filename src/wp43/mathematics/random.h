// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/random.h
 */
#if !defined(RANDOM_H)
  #define RANDOM_H

  #include <stdint.h>

  uint32_t boundedRand(uint32_t s);
  void     fnRandom   (uint16_t unusedButMandatoryParameter);
  void     fnRandomI  (uint16_t unusedButMandatoryParameter);
  void     fnSeed     (uint16_t unusedButMandatoryParameter);

#endif // !RANDOM_H
