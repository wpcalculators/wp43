// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/percentMRR.h
 */
#if !defined(PERCENTMRR_H)
  #define PERCENTMRR_H

  #include <stdint.h>

  void fnPercentMRR(uint16_t unusedButMandatoryParameter);

#endif // !PERCENTMRR_H
